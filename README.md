# Simplest indicators

Indicators for the Gnome/Unity/KDE panel, made as simple as possible.

 - **indicator-cpu-percore**: a bar-graph that shows cpu usage for each core.
 - **indicator-battery**: show battery state.

## Indicator-cpu-percore

Super simple indicator for the Unity Menu bar that show CPU usage for each
core. For each core, usage is displayed as bar, the higher the bar, the higher
is the CPU usage.

![screenshot](screenshot/indicator-cpu-percore.png)

#### Setup

 * Put the icons somewhere where the script can see them. By default, the
   location is `/usr/share/icons/simplest-indicators`. You may change this
   location by editing variable `PATH_TO_ICONS` in the script.

 * Launch the script `indicator-cpu-percore.py`



## Indicator-battety

The indicators shows the battery level and a lightning is displayed when
charging. Full ACPI informations including the estimated time left is displayed
by clicking on the indicator.

![screenshot](screenshot/indicator-battery.png)
![screenshot](screenshot/indicator-battery-clic.png)

#### Setup

 * Put the icons somewhere where the script can see them. By default, the
   location is `/usr/share/icons/simplest-indicators`. You may change this
   location by editing variable `PATH_TO_ICONS` in the script.

 * Launch the script `indicator-battery.py`


## Troubleshooting

 * If you get an error message like this
   ```
   ValueError: Namespace AppIndicator3 not available
   ```
   Try something like this
   ```
   $ sudo apt install gir1.2-appindicator3-0.1
   ```

 * The script `indicator-battery.sh` uses the `acpi` command. If this command
   is not found on your system, try something like 
   ```
   $ sudo apt install acpi
   ```
   



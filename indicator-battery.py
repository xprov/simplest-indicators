#!/usr/bin/env python3
# -*- coding: utf-8-unix -*-

# Copyright (C) <year>  <name of author>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

#
# Gnome/KDE indicator for battery state.
#
# This script uses the `acpi` command from the `acpi` package. On some system,
# run `sudo apt install acpi` to install.
#
# This applicator only show an icon without text because I haven't been able to 
# to use labels. I don't understand what's going on. The `set_label` function
# doesn't work.
#
# Battery state, pecentage and estimated time left is given by clicing on the icon.
#
# Thanks to Tim's Exploration Journal for tutorial 
#   http://candidtim.github.io/appindicator/2014/09/13/ubuntu-appindicator-step-by-step.html
# Documentation : 
#   https://lazka.github.io/pgi-docs/

import sys
import gi
import signal
import subprocess
from math import floor

gi.require_version('AppIndicator3', '0.1')
gi.require_version('Gtk', '3.0')
from gi.repository import Gtk as gtk
from gi.repository import GLib as glib
from gi.repository import AppIndicator3 as appindicator
from gi.repository import GObject as gobject

SLEEP_TIME = 1    # delay between update
APPINDICATOR_ID = 'indicator-battery'
PATH_TO_ICONS = '/usr/share/icons/simplest-indicators/'

# The first key is the battery status got from a textual analysis of the output of
# the `acpi` command.
# The second key is the threshold at which the icon is changed. For instance, icon 
# 'battery-low-charging-symbolic.svg' is used for battery percentage in interval [5, 33[.
# 'battery-empty-symbolic.svg' is used for battery percentage in interval [5, 10[.
ICONS = {
        'charging' : {
            5   : 'battery-caution-charging-symbolic.svg',
            33  : 'battery-low-charging-symbolic.svg',
            70  : 'battery-good-charging-symbolic.svg',
            100 : 'battery-full-charging-symbolic.svg',
            101 : 'battery-full-charged-symbolic.svg',
            },
        'discharging' : {
            5   : 'battery-caution-symbolic.svg',
            10  : 'battery-empty-symbolic.svg',
            33  : 'battery-low-symbolic.svg',
            70  : 'battery-good-symbolic.svg',
            101 : 'battery-full-symbolic.svg',
            },
        'unknown' : {
            101 : 'battery-missing-symbolic.svg',
            },
        }



class MyIndicator :

    def __init__(self, sleepTime):
        self.sleepTime = sleepTime # between to updates

        # Build menu that only allows to quit
        menu = gtk.Menu()

        self.menu_label = gtk.MenuItem(label='')
        menu.append(self.menu_label)
        self.menu_label.show()

        menu_item = gtk.MenuItem(label='Quit')
        menu.append(menu_item)
        menu_item.connect('activate', gtk.main_quit)
        menu_item.show()

        # Build indicator
        self.actualIcon = 'battery-missing-symbolic.svg'
        self.indicator = appindicator.Indicator.new(APPINDICATOR_ID, 
                PATH_TO_ICONS + self.actualIcon,
                appindicator.IndicatorCategory.SYSTEM_SERVICES)
        self.indicator.set_status(appindicator.IndicatorStatus.ACTIVE)
        self.indicator.set_menu(menu)

        # repeat update periodically
        glib.timeout_add_seconds(self.sleepTime, self.poll_timeout)


    def poll_timeout(self):
        try :
            self.update_self()
        except IOError :
            pass
        return True

    def update_self( self ):
        text = ''
        icon = 'battery-missing-symbolic.svg'
        try:
            acpitext = subprocess.getoutput('acpi')
            
            # acpi might display several battery even if the laptop has only
            # one. Don't know why, don't want to know why.
            # It seems that fake batteries are always displayed as
            # `Discharging, 0%, rate information unavailable`
            # Let's filter that out.
            acpitext = next(filter(lambda x : 'Discharging, 0%, rate information unavailable' not in x, acpitext.split('\n')))

            # Add informations to menu
            text = acpitext.split(': ')[1]

            # Get percentage by identifying the '%' symbol.
            percentage = int(next(filter(lambda x : '%' in x, text.split(' '))).split('%')[0])

            # Set icon
            icon = ''
            icons_list = ICONS['unknown']

            # select icon's catalogue according to battery status
            if 'Charging' in text:
                icons_list = ICONS['charging']
            elif 'Discharging' in text:
                icons_list = ICONS['discharging']

            for maxValue in icons_list:
                if percentage < maxValue:
                    icon = icons_list[maxValue]
                    break

            # update menu's text
            self.menu_label.set_label(text)
            if icon != self.actualIcon:
                self.indicator.set_icon_full(PATH_TO_ICONS + icon, text)
                self.actualIcon = icon


        except:
            pass



def main():
    signal.signal(signal.SIGINT, signal.SIG_DFL)
    myIndicator = MyIndicator(SLEEP_TIME)
    gtk.main()

if __name__ == '__main__':
    main()

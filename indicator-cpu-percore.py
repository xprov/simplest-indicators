#!/usr/bin/env python3
# -*- coding: utf-8-unix -*-

# Copyright (C) <year>  <name of author>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

#
# Gnome/KDE indicator for CPUs load and network usage.
#
# Thanks to Tim's Exploration Journal for tutorial 
#   http://candidtim.github.io/appindicator/2014/09/13/ubuntu-appindicator-step-by-step.html
# Documentation : 
#   https://lazka.github.io/pgi-docs/

import sys
import gi
import signal
import psutil # cpu_count(), cpu_percent(interval=<delay>, percpu=True)
from math import floor

gi.require_version('AppIndicator3', '0.1')
gi.require_version('Gtk', '3.0')
from gi.repository import Gtk as gtk
from gi.repository import GLib as glib
from gi.repository import AppIndicator3 as appindicator
from gi.repository import GObject as gobject

SLEEP_TIME = 1    # delay between update
SCAN_TIME = 0.1   # at each update, cpu usage checked over this period of time
APPINDICATOR_ID = 'indicator-cpu-percore'
PATH_TO_ICONS = '/usr/share/icons/simplest-indicators'

class MyIndicator :

    def __init__(self, sleepTime, scanTime ) :
        self.sleepTime = sleepTime # between to updates
        self.scanTime = scanTime # delay for scanning cpu usage

        # launch cpu usage indicator
        self.NBCPU = psutil.cpu_count()
        self.cpuindicators = []
        self.previousScales = [0] * self.NBCPU

        menu = gtk.Menu()
        menu_item = gtk.MenuItem(label='Quit')
        menu.append(menu_item)
        menu_item.connect("activate", gtk.main_quit)
        menu_item.show()
    
        for i in range ( self.NBCPU ) :
            indicator = appindicator.Indicator.new(APPINDICATOR_ID, 
                    PATH_TO_ICONS + '/indicator-cpu-percore-0.png', 
                    appindicator.IndicatorCategory.SYSTEM_SERVICES)
            indicator.set_status(appindicator.IndicatorStatus.ACTIVE)
            indicator.set_menu(menu)
            self.cpuindicators.append(indicator)

        # repeat update periodically
        #gobject.timeout_add( self.sleepTime, self.poll_timeout)
        glib.timeout_add_seconds(self.sleepTime, self.poll_timeout)


    def poll_timeout(self):
        try :
            self.update_ui()
        except IOError :
            pass
        return True

    # The scale for cpu use is 12 because there are 13 images from
    # ``indicator-cpu-percore-0.png`` to ``indicator-cpu-percore-12.png``
    # 0 means no work, 12 means 100% work.
    def update_ui( self ) :
        loads = psutil.cpu_percent(interval=self.scanTime, percpu=True)
        scales = list(map(lambda x : floor(12*x/100), loads))
        for indicator, scale, previousScale in zip(self.cpuindicators, scales, self.previousScales):
            if scale != previousScale:
                indicator.set_icon_full( PATH_TO_ICONS + \
                        "/indicator-cpu-percore-" + str(scale) + ".png", str(scale))
        self.previousScales = scales


def main():
    signal.signal(signal.SIGINT, signal.SIG_DFL)
    myIndicator = MyIndicator(SLEEP_TIME, SCAN_TIME)
    gtk.main()

if __name__ == "__main__":
    main()
